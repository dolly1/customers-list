module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
    "eslint:recommended",
    "plugin:react/recommended"
    ],
     "settings": {
    "react": {
      "version": "16.8.2"
    }
  },
    "globals": {
        "process": true,

        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parser": "babel-eslint",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "react/jsx-uses-react": "error",
        "react/jsx-uses-vars": "error"
    }
};