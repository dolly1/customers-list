import React, { Component } from 'react';
import { Navbar, Nav, Container} from 'react-bootstrap';

class Header extends Component {
  render() {
    return (
      <Container>
        <Navbar expand="lg" bg="light" variant="light" sticky="top">
          <Navbar.Brand href="/">Site Name</Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
          <Nav className="justify-content-end" activeKey="/">
            <Nav.Item>
             <Nav.Link href="/">Home</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link  href="/Customers">Customer</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link  href="/ContactUs">Contact Us</Nav.Link>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
        </Navbar>
      </Container>
    );
  }
}

export default Header;