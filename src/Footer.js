import React from 'react'
import { Navbar, Container} from 'react-bootstrap';
class Footer extends React.Component {
  render() {
    return (
      <Container>
         <Navbar expand="lg" bg="light" variant="light" sticky="bottom"  >
            &copy; Company Theme. All Rights Reserved.
        </Navbar>
      </Container>
    )
  }
}
export default Footer