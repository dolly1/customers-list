import React from 'react'
import axios from 'axios'
import { Button, Table, Container, Row, Col } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faEye, faTrash } from '@fortawesome/free-solid-svg-icons'
import ConfirmModel from './ConfirmModel'
import PropTypes from 'prop-types'

export const url = process.env.REACT_APP_PATH +"Customers/";
class Customer extends React.Component {
  constructor (props) {
    super(props)
    this.state = { customers: [],
      isOpen: false,
      id: 0,
      history:this.props.history
    }

    this.handleClose = this.handleClose.bind(this)
    this.handleShow = this.handleShow.bind(this)
  }

  handleClose () {
    this.setState({
      isOpen: false
    })
  }

  handleShow (cid) {
    this.setState({
      isOpen: true,
      id: cid
    })
  }

  async componentDidMount () {
    return axios.get(url).then(data => {
      this.setState({ customers: data.data })
    }).catch(e => e);
  }

  async deleteCustomer (id, history, customers) {

    try
    {
      this.handleClose();
      axios.delete(url + id);
      const index = customers.findIndex(customer => customer.id === id)
      customers.splice(index, 1)
      this.history.push('/Customers')
    }
    catch(err)
    {

    };
  }

  render () {
    return (
        <>
       <Container className ="pt-5">
         <Row className ="py-3">
           <Col><h2>Customers</h2></Col>
           <Col ><Button variant="outline-success" className="float-right" href="/Customers/new">Add New</Button></Col>
         </Row>
         {this.state.customers.length === 0 && (
           <Container className="text-center">
             <h2>No customer found at the moment</h2>
           </Container>
         )}
         {this.state.customers.length !== 0 && (
           <Table striped bordered hover className="text-center">
             <thead>
               <tr>
                 <th>Name</th>
                 <th>Email</th>
                 <th>Contact No</th>
                 <th>Gender</th>
                 <th>Actions</th>
               </tr>
             </thead>
             <tbody>
               {this.state.customers && this.state.customers.map(customer =>
                 <tr key={customer.id}>
                   <td><a href={`Customers/${customer.id}`}>{customer.name}</a></td>
                   <td>{customer.email}</td>
                   <td>{customer.contact_no}</td>
                   <td>{customer.gender}</td>
                   <td>
                     <Button variant="info" className ="ml-2" href={`Customers/${customer.id}`}><FontAwesomeIcon icon={faEye} /> </Button>
                     <Button variant="secondary" className ="ml-2" href={`Customers/${customer.id}/edit`} ><FontAwesomeIcon icon={faEdit} />  </Button>
                     <Button variant="danger" onClick={() => this.handleShow(customer.id)} className ="ml-2" ><FontAwesomeIcon icon={faTrash} /></Button>
                   </td>
                 </tr>
               )}
             </tbody>
           </Table>
         )}
         <ConfirmModel isOpen={this.state.isOpen} handleClose={this.handleClose} deleteCustomer={this.deleteCustomer} id={this.state.id}
           history={this.props.history} customers={this.state.customers}/>
       </Container>
     </>
    )
  }
}

Customer.propTypes = {
  history : PropTypes.any
}
export default Customer
