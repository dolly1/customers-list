import React from 'react';
import { Container } from 'react-bootstrap';
function Notfound() {
  return (
      <Container className="text-center pt-5">
      <h1>Page Not found</h1>
    </Container>
  );
}

export default Notfound;
