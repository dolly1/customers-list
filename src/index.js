import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js';
import App from './App';
import Header from './Header';
import Footer from './Footer';
import Customer from './Customer'
import CreateCustomer from './CreateCustomer'
import ViewCustomer from './ViewCustomer'
import Notfound from './Notfound'
import Contact from './Contact'


const routing = (
  <Router>
    <div>
      <Switch>
        <Route exact path="/" component={App} />
        <Route exact path="/Customers" component={Customer} />
        <Route path={'/Customers/New'} component={CreateCustomer} />
        <Route path={'/Customers/:id/edit'} component={CreateCustomer} />
        <Route path={'/Customers/:id'} component={ViewCustomer} />
        <Route path="/ContactUs" component={Contact} />
        <Route component={Notfound} />
      </Switch>
    </div>
  </Router>
)

ReactDOM.render(<Header/>, document.getElementById('header_id'));
ReactDOM.render(routing, document.getElementById('root'));
ReactDOM.render(<Footer/>, document.getElementById('footer_id'));

