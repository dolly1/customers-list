import React from 'react'
import { Container } from 'react-bootstrap'
function App () {
  return (
    <Container className="text-center pt-5">
      <h1>Welcome to Customer Application</h1>
    </Container>
  )
}

export default App
