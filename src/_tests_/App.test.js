import React from 'react';
import { shallow,configure } from 'enzyme';
import App from '../App.js';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe("<App />", () => {
  it("should render my component", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.exists()).toBe(true)
    expect(wrapper.find('h1').text()).toEqual("Welcome to Customer Application");

  });
});