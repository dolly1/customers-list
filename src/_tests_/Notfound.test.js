import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import Notfound from '../Notfound.js';

let container;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container = null;
});


it('Notfound Component', () => {
  act(() => {
    ReactDOM.render(<Notfound />, container);
  });
  const h1 = container.querySelector('h1');
  expect(h1.textContent).toBe('Page Not found');
 });