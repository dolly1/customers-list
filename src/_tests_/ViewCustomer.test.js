import React from 'react';
import { shallow,configure,componentDidMount,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'regenerator-runtime/runtime';
import App, {url} from '../ViewCustomer';
import ConfirmModel from '../ConfirmModel'
import {  Modal } from 'react-bootstrap'
import findDOMNode from 'react-dom'

configure({ adapter: new Adapter() });


const axios = require('axios');
const viewurl = url + "4";


jest.mock('axios', () => {
  const Customers = [
    { "name": "dolly",
      "email": "dolly@improwised.com",
      "contact_no": "9228286922",
      "country": "India",
      "pan_no": "awjpe2345p",
      "url": "https://google.com",
      "gender": "female",
      "languages": "english,gujarati",
      "address": "asdasd",
      "id": 4 }
  ];
  return {
    get: jest.fn((viewurl) => {
        return Promise.resolve({
          data: Customers
        });
    }),
  };
});


const app = shallow(<App  required={true} match={{params: {id: 4}, isExact: true, path: "/Customers/:id", url: "/Customers/4"}}/>);
it('fetch customer on #componentDidMount',async (done)=> {
 app
  .instance()
  .componentDidMount()
  .then(() => {
    expect(axios.get).toHaveBeenCalled();
    expect(axios.get).toHaveBeenCalledWith(viewurl);
    expect(app.state()).toHaveProperty('customers',[ { name: 'dolly',
           email: 'dolly@improwised.com',
           contact_no: '9228286922',
           country: 'India',
           pan_no: 'awjpe2345p',
           url: 'https://google.com',
           gender: 'female',
           languages: 'english,gujarati',
           address: 'asdasd',
           id: 4 } ]);


      //check data table html
      expect(app.childAt(2).html()).toEqual('<table style="font-size:20px;width:50%" class="table table-borderless"><tbody><tr><td>Name</td><td></td></tr><tr><td>Email</td><td><a href="mailto:{customers.email}"></a></td></tr><tr><td>Contact No</td><td><a href="tel:{customers.contact_no}"></a></td></tr><tr><td>Gender</td><td></td></tr><tr><td>Country</td><td></td></tr><tr><td>Languages</td><td></td></tr><tr><td>Pan No</td><td></td></tr><tr><td>URL</td><td><a></a></td></tr><tr><td>Address</td><td></td></tr></tbody></table>');

      //Check edit link
      const Ebutton = app.find('Button').at(1);
      expect(Ebutton.props()["href"]).toEqual("4/edit")

      done();

    });
});

it('Check Back link',()=> {
  const button = app.find('Button').at(0);
  expect(button.length).toBe(1);
  expect(button.props()["href"]).toEqual("/Customers")
});


it('Check delete link',()=> {
  app.find('Button').at(2).simulate('click');
  expect(app.state().isOpen).toBe(true);
  const wrapper = shallow(<ConfirmModel />);
  expect(wrapper.childAt(0).html()).toEqual('<div class="modal-header"><div class="modal-title h4">Are you sure ?</div><button type="button" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button></div>');
  expect(wrapper.childAt(1).html()).toEqual('<div class="modal-body">Would you like to delete this customer?</div>');
  expect(wrapper.childAt(2).html()).toEqual('<div class="modal-footer"><button type="button" class="btn btn-secondary">Cancel</button><button type="button" class="btn btn-primary"> Ok </button></div>');
});







